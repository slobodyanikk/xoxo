function checkGameStatus(gameField) {
    // check rows
    for (let i = 0; i < 3; i++) {
        if (gameField[i][0] && gameField[i][0] === gameField[i][1] && gameField[i][1] === gameField[i][2]) {
            return`${gameField[i][0] === 'x' ? 'крестики' : 'нолики'} победили`;
        }
    }
    // check columns
    for (let i = 0; i < 3; i++) {
        if (gameField[0][i] && gameField[0][i] === gameField[1][i] && gameField[1][i] === gameField[2][i]) {
            return`${gameField[0][i] === 'x' ? 'крестики' : 'нолики'} победили`;
        }
    }
    // check diagonals
    if (gameField[0][0] && gameField[0][0] === gameField[1][1] && gameField[1][1] === gameField[2][2]) {
        return`${gameField[0][0] === 'x' ? 'крестики' : 'нолики'} победили`;
    }
    if (gameField[0][2] && gameField[0][2] === gameField[1][1] && gameField[1][1] === gameField[2][0]) {
        return`${gameField[0][2] === 'x' ? 'крестики' : 'нолики'} победили`;
    }
    return 'Ничья';
}

const gameField = [  ['o', null, null],
    ['x', null, 'o'],
    [null, 'o', 'o']
];
console.log(checkGameStatus(gameField));