function submit() {
    const loginField = document.getElementById("login");
    const passwordField = document.getElementById("password");

    if (!loginField.checkValidity() || !passwordField.checkValidity()) {
        alert("Неверный логин или пароль");
        return;
    }

    const loginValue = loginField.value;
    const passwordValue = passwordField.value;

    console.log(`Логин: ${loginValue}, пароль: ${passwordValue}`);

    loginField.value = "";
    passwordField.value = "";
}