class GameField {
    constructor() {
        this.state = [[null, null, null], [null, null, null], [null, null, null],];
        this.mode = "X";
        this.isOverGame = false;
    }

    getGameFieldStatus() {
        return this.state.map((row) => row.map((cell) => cell === null ? '_' : cell).join(' '))
            .join('\n');
    }

    setMode() {
        this.mode = this.mode === "X" ? "O" : "X";
        return this.mode;
    }

    FieldCellValue(row, col) {
        if (this.state[row][col] !== null) {
            alert("Эта клетка уже занята!");
            return;
        }

        this.state[row][col] = this.mode;

        alert(`Вы поставили ${this.mode} в клетку (${row}, ${col})`);

        this.checkGameOver();

        if (!this.isOverGame) {
            this.setMode();
        }
    }

    checkGameOver() {

        // check rows
        for (let i = 0; i < 3; i++) {
            if (this.state[i][0] && this.state[i][0] === this.state[i][1] && this.state[i][1] === this.state[i][2]) {
                this.isOverGame = true;
                alert(`Игра окончена! Победитель: ${this.mode}`);
                break;
            }
        }
        // check columns
        for (let i = 0; i < 3; i++) {
            if (this.state[0][i] && this.state[0][i] === this.state[1][i] && this.state[1][i] === this.state[2][i]) {
                this.isOverGame = true;
                alert(`Игра окончена! Победитель: ${this.mode}`);
                break;
            }
        }
        // check diagonals
        if (this.state[0][0] && this.state[0][0] === this.state[1][1] && this.state[1][1] === this.state[2][2]) {
            this.isOverGame = true;
            alert(`Игра окончена! Победитель: ${this.mode}`);
        }
        if (this.state[0][2] && this.state[0][2] === this.state[1][1] && this.state[1][1] === this.state[2][0]) {
            this.isOverGame = true;
            alert(`Игра окончена! Победитель: ${this.mode}`);
        }

        const isBoardFull = this.state.every((row) => row.every((cell) => cell !== null));
        if (isBoardFull) {
            this.isOverGame = true;
            alert("Игра окончена! Ничья!");
        }
    }
}

const game = new GameField();

while (!game.isOverGame) {
    alert(game.getGameFieldStatus());
    alert(`Ход игрока: ${game.mode}`);
    const row = parseInt(prompt("Введите номер строки (от 0 до 2):"));
    const col = parseInt(prompt("Введите номер столбца (от 0 до 2):"));
    game.FieldCellValue(row, col);
}
