// Определение переменных
const board = document.querySelector('.game-board');
const cells = board.querySelectorAll('.cell');
const currentPlayerElement = document.getElementById('game-step');
let currentPlayer = 'x';

// Добавление обработчика событий на каждую ячейку
cells.forEach(cell => {
    cell.addEventListener('click', () => {
        if (cell.innerHTML !== '') {
            return;
        }

        if (currentPlayer === 'x') {
            cell.innerHTML ='<img src="../imgs/xxl-x.svg"/>'
            currentPlayer = 'o';
            currentPlayerElement.innerHTML = 'Ходит&nbsp;<img src="../imgs/zero.svg"/>&nbsp;';
        } else {
            cell.innerHTML ='<img src="../imgs/zero.svg"/>'
            currentPlayer = 'x';
            currentPlayerElement.innerHTML = 'Ходит&nbsp;<img src="../imgs/x.svg"/>&nbsp;';
        }
    });
});